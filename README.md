# [JetBrains Academy - Project: Dominoes](game.py)

## About
Have you ever wanted to code a game where the computer is your enemy? Well, this little project allows you to do just that.
Take turns playing classic dominoes against your computer in a race to victory.
Learn, how artificial intelligence can make use of simple statistics to make educated decisions. This project is all about basic concepts, put them to practice by making a fun little game.

## Learning outcomes
This project is all about basic concepts. You'll work with strings, tuples, lists, conditional statements, and more.

## Stages

### Stage 1/5: Setting Up the Game

#### Description
To play domino, you need a full domino set and at least two players. In this project, the game is played by you and the computer.

At the beginning of the game, each player is handed 7 random domino pieces. The rest are used as stock (the extra pieces).

To start the game, players determine the starting piece. The player with the highest domino or the highest double ([6,6] or [5,5] for example) will donate that domino as a starting piece for the game. After doing so, their opponent will start the game by going first. If no one has a double domino, the pieces are reshuffled and redistributed.

#### Objectives
1. Generate a full domino set. Each domino is represented as a list of two numbers. A full domino set is a list of 28 unique dominoes.
2. Split the full domino set between the players and the stock by random. You should get three parts: Stock pieces (14 domino elements), Computer pieces (7 domino elements), and Player pieces (7 domino elements). 
3. Determine the starting piece and the first player. Modify the parts accordingly. You should get four parts with domino pieces and one string indicating the player that goes first: either "player" or "computer".
   * Stock pieces      # 14 domino elements
   * Computer pieces   # 7 or 6 domino elements
   * Player pieces     # 6 or 7 domino elements
   * Domino snake      # 1 starting domino
   * Status            # the player that goes first
   
   If the starting piece cannot be determined (no one has a double domino), reshuffle, and redistribute the pieces (step 3).
4. Output all five variables.

### Stage 2/5: The Interface

#### Description
A good game needs a good interface. In this stage, you will make your output user-friendly.

The player should be able to see the domino snake, the so-called playing field, and their own pieces. It's a good idea to enumerate these pieces because throughout the game the player will be selecting them to make a move.

Two things must remain hidden from the player: the stock pieces and the computer's pieces. The player should not be able to see them, only the number of pieces remaining.

#### Objectives
1. Print the header using seventy equal sign characters (=).
2. Print the number of dominoes remaining in the stock – Stock size: [number].
3. Print the number of dominoes the computer has – Computer pieces: [number].
4. Print the domino snake. At this stage, it consists of the only starting piece.
5. Print the player's pieces, Your pieces:, and then one piece per line, enumerated.
6. Print the status of the game:
   
   If status = "computer", print "Status: Computer is about to make a move. Press Enter to continue..."
   
   If status = "player", print "Status: It's your turn to make a move. Enter your command."
   
   Note that both these statuses suppose that the next move will be made, but at this stage, the program should stop here. We will implement other statuses (like "win", "lose", and "draw") in the stages to come.

### Stage 3/5: Taking Turns

#### Description
It's time to bring the game to life. In this stage, you need to add a game loop that will allow players to take turns until the end-game condition is met.

In dominoes, you can make a move by taking one of the following actions:

* Select a domino and place it on the right side of the snake.
* Select a domino and place it on the left side of the snake.
* Take an extra piece from the stock (if it's not empty) and skip a turn.

To make a move, the player has to specify the action they want to take. In this project, the actions are represented by integer numbers in the following manner: {side_of_the_snake (+/-), domino_number (integer)} or {0}. For example:

* -6 : Take the sixth domino and place it on the left side of the snake. 
* 6 : Take the sixth domino and place it on the right side of the snake.
* 0 : Take an extra piece from the stock (if it's not empty) and skip a turn.

When it's time for the player to make a move, your program must prompt the user for a number. If this number exceeds the limitations (larger than the number of dominoes), your program must generate an error message and prompt for input again. Once the valid input is received, your program must apply the move.

For now, don't bother about the AI, our goal is just to make the game playable. So, when it's time for the computer to make a move, make it choose a random number between -computer_size and computer_size (where the computer_size is the number of dominoes the computer has).

The end-game condition can be achieved in two ways:

1. One of the players runs out of pieces. The first player to do so is considered a winner.
2. The numbers on the ends of the snake are identical and appear within the snake 8 times. For example, the snake bellow will satisfy this condition:

   [5,5],[5,2],[2,1],[1,5],[5,4],[4,0],[0,5],[5,3],[3,6],[6,5]
   
   These two snakes, however, will not:
   
   [5,5],[5,2],[2,1],[1,5],[5,4],[4,0],[0,5]
   
   [6,5],[5,5],[5,2],[2,1],[1,5],[5,4],[4,0],[0,5],[5,3],[3,1]
   
   If this condition is satisfied, it is no longer possible to go on with this snake. Even after emptying the stock, no player will have the necessary piece. Essentially, the game has come to a permanent stop, so we have a draw.

When the game ends, your program should print the result.

Throughout the gameplay, the snake will grow in length. If it gets too large, the interface might get ugly. To avoid this problem, draw only the first and the last three pieces of the snake, separate them by three dots, ..., for example, [3, 5][2, 2][6, 6]...[3, 6][0, 3][3, 4].

#### Objectives

Modify your Stage 2 code:

1. At the end of the game, print one of the following phrases:

   Status: The game is over. You won!

   Status: The game is over. The computer won!

   Status: The game is over. It's a draw!

2. Print only the first and the last three pieces of the domino snake separated by three dots if it exceeds six dominoes in length.

3. Add a game loop that will repeat the following steps until the game ends:
   * Display the current playing field (stage 2). 
   * If it's a user's turn, prompt the user for a move and apply it. If the input is invalid (a not-integer or it exceeds limitations), request a new input with the following message: Invalid input. Please try again.. 
   * If it's a computer's turn, prompt the user to press Enter, randomly generate a move, and apply it. 
   * Switch turns.

Keep in mind that at this stage we have no rules! Both the player and the computer can place their dominoes however they like.

### Stage 4/5: Enforcing Rules

#### Description

You can't have a game without rules. It's time to introduce them!

Until now, the players were able to place their dominoes however they like. Now, it is considered a violation. According to the rules, the numbers on the ends of the two neighboring dominoes must match each other. This rule can also be described as a set of two requirements:

1. A player cannot add a domino to the end of the snake if it doesn't contain the matching number.
2. The orientation of the newly added domino ensures that the matching numbers are neighbors.

For example, consider the following situation:

We have a [3,4],[4,4],[4,2] snake and a [1,2] domino. The domino cannot be added to the left side of the snake because there is no 3 in [1,2]. However, the domino can be added to the right side of the snake because [1,2] contains a 2. If we were to place the domino on the right side of the snake, we would have to reorient it: [3,4],[4,4],[4,2],[2,1].

These two requirements are strict for both the player and the computer.

#### Objectives

Add the following functionality to your code. When it's a player's turn, the program should:

1. Verify that the move entered by the player is legal (requirement #1).
If not, request a new input with the following message: Illegal move. Please try again..
2. Place dominoes with the correct orientation (requirement #2).

When it's a computer's turn, the program should:

1. Try random moves until it finds a legal one.
   * A set of possible moves ranges from -computer_size to computer_size (where the computer_size is the number of dominoes the computer still has). Skipping a turn (move 0) is always legal.
3. Place dominoes with the correct orientation.

### Stage 5/5: The AI

#### Description

Randomly made choices are hardly a sign of intelligence. Teach your computer to make educated decisions with the help of basic statistics. Here's how it works:

The primary objective of the AI is to determine which domino is the least favorable and then get rid of it. To reduce your chances of skipping a turn, you must increase the diversity of your pieces. For example, it's unwise to play your only domino that has a 3, unless there's nothing else that can be done. Using this logic, the AI will evaluate each domino in possession, based on the rarity. Dominoes with rare numbers will get lower scores, while dominoes with common numbers will get higher scores.

The AI should use the following algorithm to calculate the score:

1. Count the number of 0's, 1's, 2's, etc., in your hand, and in the snake.
2. Each domino in your hand receives a score equal to the sum of appearances of each of its numbers.

The AI will now attempt to play the domino with the largest score, trying both the left and the right sides of the snake. If the rules prohibit this move, the AI will move down the score list and try another domino. The AI will skip the turn if it runs out of options.

#### Objectives

Replace the random move generator with the algorithm.
