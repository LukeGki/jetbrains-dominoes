import random


class Domino:
    def __init__(self):
        self.domino_set = self.create_domino_set()
        self.playing_sets = self.create_playing_sets(self.domino_set)
        self.stock_set = self.playing_sets[0]
        self.player_set = self.playing_sets[1]
        self.computer_set = self.playing_sets[2]
        self.domino_snake = self.playing_sets[3]
        self.status = self.playing_sets[4]
        if self.status == "player":
            self.set_to_use = self.player_set
        elif self.status == "computer":
            self.set_to_use = self.computer_set
        self.game_finished = False
        self.winner = None
        self.run()

    def create_domino_set(self):
        domino_set = []
        for first_number in range(0, 7):
            for second_number in range(first_number, 7):
                domino_set.append([first_number, second_number])
        return domino_set

    def create_playing_sets(self, domino_set):
        while True:
            stock_set = domino_set.copy()
            player_set = []
            computer_set = []
            domino_snake = []
            status = None

            for i in range(7):
                domino_to_add = random.choice(stock_set)
                player_set.append(domino_to_add)
                stock_set.remove(domino_to_add)
                domino_to_add = random.choice(stock_set)
                computer_set.append(domino_to_add)
                stock_set.remove(domino_to_add)

            for number in range(6, 0, -1):
                if [number, number] in player_set:
                    domino_snake.append([number, number])
                    player_set.remove([number, number])
                    status = "computer"
                    break
                elif [number, number] in computer_set:
                    domino_snake.append([number, number])
                    computer_set.remove([number, number])
                    status = "player"
                    break
            else:
                continue
            break

        return [stock_set, player_set, computer_set, domino_snake, status]

    def print_other_pieces(self):
        print("=" * 70)
        print("Stock size: ", len(self.stock_set))
        print("Computer pieces: ", len(self.computer_set))

    def print_domino_snake(self):
        if len(self.domino_snake) > 6:
            print(*self.domino_snake[:3], "...", *self.domino_snake[-3:], sep="")
        else:
            print(*self.domino_snake, sep="")

    def print_your_pieces(self):
        print("Your pieces:")
        numeration = 1
        for piece in self.player_set:
            print(numeration, ":", piece, sep="")
            numeration += 1

    def rotate_domino(self, domino):
        return [domino[1], domino[0]]

    def make_move(self, command):
        if command == 0:
            if len(self.stock_set) > 0:
                domino = random.choice(self.stock_set)
                self.stock_set.remove(domino)
                self.set_to_use.append(domino)
        elif command > 0:
            domino = self.set_to_use[command - 1]
            self.set_to_use.remove(domino)
            if self.domino_snake[-1][-1] == domino[0]:
                self.domino_snake.append(domino)
            elif self.domino_snake[-1][-1] == domino[1]:
                self.domino_snake.append(self.rotate_domino(domino))
        elif command < 0:
            domino = self.set_to_use[abs(command) - 1]
            self.set_to_use.remove(domino)
            if self.domino_snake[0][0] == domino[1]:
                self.domino_snake.insert(0, domino)
            elif self.domino_snake[0][0] == domino[0]:
                self.domino_snake.insert(0, self.rotate_domino(domino))

    def is_placing_domino_possible(self, command):
        if command > 0:
            if self.domino_snake[-1][-1] in self.set_to_use[command - 1]:
                return True
        elif command < 0:
            if self.domino_snake[0][0] in self.set_to_use[abs(command) - 1]:
                return True

    def player_move(self):
        while True:
            command = input()
            try:
                int(command)
                if -1 * len(self.player_set) <= int(command) <= len(self.player_set):
                    if int(command) != 0:
                        if not self.is_placing_domino_possible(int(command)):
                            raise Exception
                    break
                else:
                    raise ValueError
            except ValueError:
                print("Invalid input. Please try again.")
                continue
            except Exception:
                print("Illegal move. Please try again.")
                continue
        self.make_move(int(command))

    def count_the_numbers(self):
        computer_and_domino_snake = self.computer_set + self.domino_snake
        numbers_counted = {}
        for number in range(7):
            count = sum(domino.count(number) for domino in computer_and_domino_snake)
            numbers_counted[number] = count
        return numbers_counted

    def make_computer_decision(self):
        numbers_counted = self.count_the_numbers()
        dominos_value = {}
        for domino in self.computer_set:
            dominos_value[self.computer_set.index(domino)] = numbers_counted[domino[0]] + numbers_counted[domino[1]]

        for i in range(len(self.computer_set)):
            key_max = max(dominos_value, key=dominos_value.get)
            del dominos_value[key_max]
            if self.is_placing_domino_possible(key_max + 1):
                return key_max + 1
            elif self.is_placing_domino_possible(-1 * (key_max + 1)):
                return -1 * (key_max + 1)
        return 0

    def computer_move(self):
        input()
        command = self.make_computer_decision()
        self.make_move(command)

    def is_domino_snake_blocked(self):
        first_number = self.domino_snake[0][0]
        last_number = self.domino_snake[-1][-1]
        if first_number == last_number:
            if sum(domino.count(first_number) for domino in self.domino_snake) == 8:
                return True

    def check_game_condition(self):
        if len(self.player_set) == 0:
            self.status = "game finished"
            self.winner = "player"
        elif len(self.computer_set) == 0:
            self.status = "game finished"
            self.winner = "computer"
        elif self.is_domino_snake_blocked():
            self.status = "game finished"
            self.winner = "draw"

    def run(self):
        while True:
            self.print_other_pieces()
            print()
            self.print_domino_snake()
            print()
            self.print_your_pieces()
            print()

            if self.status == "game finished":
                if self.winner == "player":
                    print("Status: The game is over. You won!")
                elif self.winner == "computer":
                    print("Status: The game is over. The computer won!")
                elif self.winner == "draw":
                    print("Status: The game is over. It's a draw!")
                break
            elif self.status == "player":
                print("Status: It's your turn to make a move. Enter your command.")
                self.player_move()
                self.status = "computer"
                self.set_to_use = self.computer_set
            elif self.status == "computer":
                print("Status: Computer is about to make a move. Press Enter to continue...")
                self.computer_move()
                self.status = "player"
                self.set_to_use = self.player_set

            self.check_game_condition()


game = Domino()
